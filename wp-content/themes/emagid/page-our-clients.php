<?php
/**
 * The template for displaying all pages
 * 
 *  Template Name: Investments Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress constr<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: About Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1>OUR CLIENTS</h1>
            </div>
        </div>
	</section>

	<!-- HERO SECTION END -->

	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<p class="large"><?php the_field('intro'); ?></p>
		</div>

    </section>
	<!-- GROWTH SECTION END -->

    <section class='bio' id="investments_bio">

        <div class="client_logos">
            
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'posts_per_page' => 99
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
            
            <div class="logo">
                <a href="<?php the_field('external_link'); ?>">
                    <img src="<?php the_field('logo'); ?>">
                </a>
            </div>
        <?php
			}
				}
			else {
			echo 'No Clients Found';
			}
		?>  
            
    <?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>

        </div>
        
        	<!-- QUOTE SECTION -->
	<div class="quote_slider quote_page">
        
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'posts_per_page' => 3
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
        
		<div class='quotes'>
			<img src="<?php the_field('logo'); ?>">
            <div class="quote_text">
                <h3><?php the_field('company_name'); ?></h3>
                <p><?php the_field('testimonial'); ?></p>
            </div>
		</div>
        <?php
			}
				}
			else {
			echo 'No Clients Found';
			}
		?>  
<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</div>
	<!-- QUOTE SECTION END  -->
        
        
        
        <div class="client_list">
            <h2>Client List</h2>
            <div class="list">
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'posts_per_page' => 99
                    
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                <p><?php the_field('company_name'); ?></p>
        <?php
			}
				}
			else {
			echo 'No Clients Found';
			}
		?> 
            </div>
        </div>


	</section>

  <script type="text/javascript">
    $(document).ready(function(){
      $('.quote_slider').slick({
        autoplay:true,
		autoplaySpeed:4000,
		dots: true,
          arrows: true
      });
    });
  </script>


<?php
get_footer();
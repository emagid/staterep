<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>




	<!-- HERO SECTION -->
	<section class='hero portfolio_hero' style="  background-image: url(<?php the_field('banner'); ?>);">
		<div class='text_box'>
			<h1>OUR INVESTMENTS</h1>
            <p>Unleashing endless potential</p>
		</div>
	</section>

	<!-- HERO SECTION END -->

<div class="portfolio_Wrapper">
<div class="portfolio_container">
    <section class='portfolio_nav'>
        <h2>Financial</h2>
        <?php the_field('financial_quote'); ?>

    </section>

	<!-- LOGO SECTION -->
	<section class='portfolio'>
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'cat'=> '3'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
			<a href="<?php the_field('link'); ?>" target="_blank">
				 <div class='overlay'><p><?php the_field('status'); ?></p></div> 
				<img class="client_logo grey" src="<?php the_field('logo'); ?>">
			</a>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   
<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</section>
</div>

<div class="portfolio_container"> 
	<!-- LOGO SECTION END -->
        <section class='portfolio_nav'>
        <h2>Tech</h2>
            <?php the_field('tech_quote'); ?>

    </section>
	<!-- LOGO SECTION -->
	<section class='portfolio'>
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'cat'=> '6'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
			<a href="<?php the_field('link'); ?>" target="_blank">
				 <div class='overlay'><p><?php the_field('status'); ?></p></div> 
				<img class="client_logo grey" src="<?php the_field('logo'); ?>">
			</a>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   
<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</section>
	<!-- LOGO SECTION END -->
</div>
<div class="portfolio_container"> 
	<!-- LOGO SECTION END -->
        <section class='portfolio_nav'>
        <h2>Consumer</h2>
        <?php the_field('consumer_quote'); ?>
    </section>
	<!-- LOGO SECTION -->
	<section class='portfolio'>
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'cat'=> '4'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
			<a href="<?php the_field('link'); ?>" target="_blank">
				 <div class='overlay'><p><?php the_field('status'); ?></p></div> 
				<img class="client_logo grey" src="<?php the_field('logo'); ?>">
			</a>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   
<?php wp_reset_query();	 // Restore global post data stomped by the_post(). ?>
	</section>
	<!-- LOGO SECTION END -->
</div>

<div class="portfolio_container"> 
	<!-- LOGO SECTION END -->
        <section class='portfolio_nav'>
        <h2>Medical</h2>
        <?php the_field('medical_quote'); ?>
    </section>
	<!-- LOGO SECTION -->
	<section class='portfolio'>
            <?php
	  			$args = array(
	    		'post_type' => 'clients',
                    'cat'=> '5'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
			<a href="<?php the_field('link'); ?>" target="_blank">
				 <div class='overlay'><p><?php the_field('status'); ?></p></div> 
				<img class="client_logo grey" src="<?php the_field('logo'); ?>">
                     
			</a>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>   

	</section>
	<!-- LOGO SECTION END -->
</div>

</div>


<script>
    $('.portfolio a').hover(function(){
  $('.overlay', this).css('visibility', 'visible')
  },function(){
  $('.overlay', this).css('visibility', 'hidden')
  });

</script>

<?php
get_footer();

<?php
/**
 * The template for displaying all pages
 *
 *  Template Name: Front Page
 * 
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>



	<!-- HERO SECTION -->
	<section class='hero home_hero' style="background-image:url(<?php the_field('banner'); ?>)">
        <div class="overlay">
            <div class='text_box'>
                <h1><?php the_field('hero_title'); ?></h1>
                <p><?php the_field('hero_subtitle'); ?></p>
                <a href='<?php the_field('button_link'); ?>' class='button scroll_down'> <?php the_field('button_text'); ?></a>
            </div>
        </div>
	</section>
    <section class="small_hero">
            <div class='text_box'>
                <h4><?php the_field('red_banner_text'); ?></h4>
<!--                <a href="/my-account"><h4>Register</h4></a>-->
            </div>
    </section>

	<!-- HERO SECTION END -->

	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<p class="thin"><?php the_field('about'); ?></p>
		</div>

    </section>
	<!-- GROWTH SECTION END -->

<!-- ICON SECTION -->
<section class="summary_icons">
    <div class="wrapper">
        <h2>Legislation</h2>
        <div class="s_icon">
            <a href="/taxes-spending-fees/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/taxes.png">
                <p>TAXES, SPENDING, & FEES</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/education">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/hall.png">
                <p>Education</p>
            </a>
        </div>

        <div class="s_icon">
            <a href="/second-amendment/">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/rank.png">
            <p>SECOND AMENDMENT</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/labor/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/laborers.png">
                <p>Labor</p>
            </a>
        </div>
        <div class="s_icon">
            <a href="/labor/">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/policeman.png">
                <p>Law Enforcement</p>
            </a>
        </div>
    </div>
</section>

<!-- ICON SECTION END -->


<!-- MIDBANNER SECTION -->
<section class="midbanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/capital.jpeg)">
</section>

<!-- MIDBANNER SECTION END -->

	<!-- QUOTE SECTION -->
<!--
	<div class="quote_slider">
		<div class='quotes'>
			<img src="<?php the_field('eric_image'); ?>">
            <div class="quote_text">
                <h3>Eric Schleien</h3>
                <p><?php the_field('eric_text'); ?></p>
            </div>
		</div>
        <div class='quotes'>
			<img src="<?php the_field('john_image'); ?>">
            <div class="quote_text">
                <h3>John King</h3>
                <p><?php the_field('john_text'); ?></p>
            </div>
		</div>
	</div>
-->
	<!-- QUOTE SECTION END  -->


	<!-- GROWTH SECTION -->
	<section class='growth'>
		<div class="head_text">
			<h2>Latest News</h2>
		</div>
        <div class="classes">
            <div class="class">
                            <?php
	  			$args = array(
	    		'post_type' => 'news'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
                <div class="class_box">
                    <h4><?php the_field('headline'); ?></h4>
                    <p><?php the_field('snippet'); ?></p>
                    <a href='<?php the_field('link'); ?>' class='button' target="_blank"> READ MORE </a>
                </div>
        <?php
			}
				}
			else {
			echo 'No News Found';
			}
		?>   
            </div>
        </div>

    </section>
	<!-- GROWTH SECTION END -->


	<!-- AUDIOBOOK SECTION -->
	<section class='audiobook'>
        <div class="graphic">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2908.1366488774665!2d-71.5402605845167!3d43.20662312913915!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e2136e9b8c52cf%3A0x100610ccbefbf3da!2sState+House!5e0!3m2!1sen!2sus!4v1528410685337" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class="copy">
			<h1>Contact NH State Rep. Eric Schleien</h1>
            <br>
            <p>Complete the form below to email Representative Schleien.</p>
            <?php echo do_shortcode('[contact-form-7 id="30" title="Contact Form"]'); ?>
            
		</div>



    </section>
	<!-- AUDIOBOOK SECTION ENDS -->

<!-- CTABANNER SECTION -->
<!--
<section class="ctabanner" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/img/ctabanner.jpg)">
    <div class="overlay">
        <div class='text_box'>
            <a href='/about' class='button'> HOW IT WORKS</a>
        </div>
    </div>
</section>
-->

<!-- CTABANNER SECTION END -->





  <script type="text/javascript">
    $(document).ready(function(){
      $('.quote_slider').slick({
        autoplay:true,
		autoplaySpeed:2500,
		dots: true,
          arrows: true
      });
    });
  </script>
<script>
$(".scroll_down").click(function() {
    $('html,body').animate({
        scrollTop: $(".summary_icons").offset().top},
        'slow');
});
</script>
<?php
get_footer();

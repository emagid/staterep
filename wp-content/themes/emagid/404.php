<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package emagid
 */

get_header(); ?>

<div class="text-wrapper">
    <div class="title" data-content="404">
        404
    </div>

    <div class="subtitle">
        Oops, the page you're looking for doesn't exist.
    </div>

    <div class="buttons">
        <a class="button" href="/">Go to homepage</a>
    </div>
</div>

<style>

.text-wrapper {
  background-color: #b1b1b1;
  height: 100%;
    min-height: 600px;
}

.text-wrapper {
  height: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
}

.title {
  font-size: 6em;
  font-weight: 700;
  color: #298cd6;
}

.subtitle {
  font-size: 40px;
    line-height: 1.25em;
    margin-top: 40px;
    margin-bottom: 25px;
  color: #fff;
    text-align: center;
}

.buttons {
  margin: 30px;
}
.buttons a.button {
  border: 1px solid #298cd6;
    background: #298cd6;
  text-decoration: none;
    letter-spacing: 1px;
  padding: 15px;
  text-transform: uppercase;
  color: #fff;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}
.buttons a.button:hover {
  border-color: white;
  -webkit-transition: all 0.2s ease-in-out;
  transition: all 0.2s ease-in-out;
}

</style>

<?php
get_footer();
